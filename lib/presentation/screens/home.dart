import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:noted/data/models/note_model.dart';
import '../../core/constants.dart';

import 'note_list.dart';

enum ViewOrientation { grid, column }

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool gridView = false;
  NoteModel note = NoteModel.dummy();

  final titleController = TextEditingController();
  final noteController = TextEditingController();
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<void> _addNote() {
    final title = titleController.text;
    final note = noteController.text;
    final created = DateTime.now();
    final notes = firestore.collection("notes");
    // if (title.isNotEmpty && note.isNotEmpty) {
    return notes
        .add({"title": title, "note": note, "created": created})
        .then((value) =>
            Fluttertoast.showToast(msg: "Note added ${notes.doc().id}"))
        .catchError((val) => Fluttertoast.showToast(msg: "failed with :$val"));
    // }

    // throw Exception("Title and note cannot be empty");
  }

  @override
  void initState() {
    super.initState();
  }

  void _toggleView() {
    setState(() {
      gridView = !gridView;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(
          widget.title,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 45,
          ),
        ),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: InkWell(
                onTap: _toggleView,
                hoverColor: Colors.blueGrey,
                child: MediaQuery.of(context).size.width > 430
                    ? gridView
                        ? const Icon(
                            Icons.grid_view_outlined,
                            size: 35,
                          )
                        : const Icon(Icons.list, size: 35)
                    : const Text("")),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: InkWell(
              onTap: () {
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  builder: (BuildContext context) {
                    return SizedBox(
                      height: 620,
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: 8,
                            left: 8,
                            right: 8,
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              const Text(
                                'Add New Note',
                                style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(height: 20.0),
                              TextField(
                                controller: titleController,
                                decoration: const InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    hintText: "Enter note title"),
                              ),
                              const SizedBox(height: 20.0),
                              TextField(
                                controller: noteController,
                                decoration: const InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8)),
                                    ),
                                    hintText: "Enter note text"),
                              ),
                              const SizedBox(height: 20.0),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  ElevatedButton(
                                    child: const Text('Add Note'),
                                    onPressed: () {
                                      if (titleController.text.isNotEmpty &&
                                          noteController.text.isNotEmpty) {
                                        _addNote();
                                        titleController.clear();
                                        noteController.clear();
                                        Navigator.pop(context);
                                      }
                                      Fluttertoast.showToast(
                                          toastLength: Toast.LENGTH_LONG,
                                          msg:
                                              "Please fill in the note Title and note details");
                                    },
                                  ),
                                  ElevatedButton(
                                    child: const Text('Cancel'),
                                    onPressed: () {
                                      titleController.clear();
                                      noteController.clear();
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
              hoverColor: Colors.blueGrey,
              child: Row(
                children: const [Text("New Note"), Icon(Icons.add_outlined)],
              ),
            ),
          )
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(children: <Widget>[
            AppSpacing.v24,
            NoteList(
              gridOption: gridView,
            ),
          ]),
        ),
      ),
    );
  }
}
