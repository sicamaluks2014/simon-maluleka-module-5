import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class NoteList extends StatelessWidget {
  final bool gridOption;
  NoteList({Key? key, required this.gridOption}) : super(key: key);

  final Stream<QuerySnapshot> _notesStream =
      FirebaseFirestore.instance.collection('notes').snapshots();
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  final titleController = TextEditingController();
  final noteController = TextEditingController();

  Future<void> _updateNote(String doc) {
    final title = titleController.text;
    final note = noteController.text;
    final notes = firestore.collection("notes");

    return notes
        .doc(doc)
        .update({"title": title, "note": note})
        .then((value) =>
            Fluttertoast.showToast(msg: "Note updated ${notes.doc().id}"))
        .catchError((val) => Fluttertoast.showToast(msg: "failed with :$val"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _notesStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: Text(
                "Loading...",
                style: TextStyle(fontSize: 50, color: Colors.blueGrey),
              ),
            );
          }

          return Expanded(
            child: gridOption
                ? ListView(
                    children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;

                      return Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.circular(10), // if you need this
                          side: BorderSide(
                            color: Colors.grey.withOpacity(0.2),
                            width: 1,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Stack(children: [
                            ListTile(
                              textColor: Colors.blueGrey,
                              title: Text(data['title']),
                              subtitle: Text(data['note']),
                              //trailing: Text(data['created'].toString()),
                            ),
                            Positioned(
                              top: 5,
                              right: 2,
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      titleController.text = data['title'];
                                      noteController.text = data['note'];
                                      showModalBottomSheet<void>(
                                        isScrollControlled: true,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return SizedBox(
                                            height: 620,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 8,
                                                  left: 8,
                                                  right: 8,
                                                  bottom: MediaQuery.of(context)
                                                      .viewInsets
                                                      .bottom),
                                              child: Center(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    const Text(
                                                      'Edit Note',
                                                      style: TextStyle(
                                                          fontSize: 30,
                                                          color:
                                                              Colors.blueGrey,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    const SizedBox(height: 10),
                                                    TextField(
                                                      controller:
                                                          titleController,
                                                      decoration:
                                                          const InputDecoration(
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            8)),
                                                              ),
                                                              hintText:
                                                                  "Enter note title"),
                                                    ),
                                                    const SizedBox(height: 10),
                                                    TextField(
                                                      controller:
                                                          noteController,
                                                      decoration:
                                                          const InputDecoration(
                                                              border:
                                                                  OutlineInputBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            8)),
                                                              ),
                                                              hintText:
                                                                  "Enter note text"),
                                                    ),
                                                    const SizedBox(height: 10),
                                                    ElevatedButton(
                                                      child: const Text(
                                                          'Save Note'),
                                                      onPressed: () {
                                                        if (titleController.text
                                                                .isNotEmpty &&
                                                            noteController.text
                                                                .isNotEmpty) {
                                                          _updateNote(
                                                              document.id);
                                                          titleController
                                                              .clear();
                                                          noteController
                                                              .clear();
                                                          Navigator.pop(
                                                              context);
                                                        }
                                                        Fluttertoast.showToast(
                                                            toastLength: Toast
                                                                .LENGTH_LONG,
                                                            msg:
                                                                "Please fill in the note Title and note details");
                                                      },
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    child: const Icon(
                                      Icons.edit_outlined,
                                      size: 35,
                                      color: Colors.green,
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      FirebaseFirestore.instance
                                          .collection("notes")
                                          .doc(document.id)
                                          .delete();
                                    },
                                    child: const Icon(
                                      Icons.delete_outlined,
                                      size: 35,
                                      color: Colors.red,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Positioned(
                                bottom: 2,
                                right: 5,
                                child: Text(
                                    style: const TextStyle(
                                        fontSize: 20, color: Colors.blueGrey),
                                    "Added: ${DateFormat('yyyy-MM-dd').format(DateTime.parse(data['created'].toDate().toString()))}"))
                          ]),
                        ),
                      );
                    }).toList(),
                  )
                : GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 450,
                            childAspectRatio: 3 / 2,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20),
                    itemCount: snapshot.data!.docs.length,
                    itemBuilder: (BuildContext ctx, index) {
                      return Card(
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(10), // if you need this
                            side: BorderSide(
                              color: Colors.grey.withOpacity(0.2),
                              width: 1,
                            ),
                          ),
                          child: SizedBox(
                            width: 300,
                            height: 300,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 30),
                                    child: ListTile(
                                      textColor: Colors.blueGrey,

                                      title: Text(
                                        snapshot.data!.docs[index]['title'],
                                        style: const TextStyle(
                                            fontSize: 30,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Text(
                                          "${snapshot.data!.docs[index]['note']}"),
                                      // trailing:
                                      //     Text(notes[index].created.toLocal().toString()),
                                    ),
                                  ),
                                  Positioned(
                                      top: 1,
                                      right: 2,
                                      child: Row(
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              titleController.text = snapshot
                                                  .data!.docs[index]['title'];
                                              noteController.text = snapshot
                                                  .data!.docs[index]['note'];
                                              showModalBottomSheet<void>(
                                                isScrollControlled: true,
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return SizedBox(
                                                    height: 620,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 8,
                                                          left: 8,
                                                          right: 8,
                                                          bottom: MediaQuery.of(
                                                                  context)
                                                              .viewInsets
                                                              .bottom),
                                                      child: Center(
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          children: <Widget>[
                                                            const Text(
                                                              'Edit Note',
                                                              style: TextStyle(
                                                                  fontSize: 40,
                                                                  color: Colors
                                                                      .blueGrey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                            const SizedBox(
                                                                height: 20.0),
                                                            TextField(
                                                              controller:
                                                                  titleController,
                                                              decoration:
                                                                  const InputDecoration(
                                                                      border:
                                                                          OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(8)),
                                                                      ),
                                                                      hintText:
                                                                          "Enter note title"),
                                                            ),
                                                            const SizedBox(
                                                                height: 20.0),
                                                            TextField(
                                                              controller:
                                                                  noteController,
                                                              decoration:
                                                                  const InputDecoration(
                                                                      border:
                                                                          OutlineInputBorder(
                                                                        borderRadius:
                                                                            BorderRadius.all(Radius.circular(8)),
                                                                      ),
                                                                      hintText:
                                                                          "Enter note text"),
                                                            ),
                                                            const SizedBox(
                                                                height: 20.0),
                                                            ElevatedButton(
                                                              child: const Text(
                                                                  'Save Note'),
                                                              onPressed: () {
                                                                if (titleController
                                                                        .text
                                                                        .isNotEmpty &&
                                                                    noteController
                                                                        .text
                                                                        .isNotEmpty) {
                                                                  _updateNote(
                                                                      snapshot
                                                                          .data!
                                                                          .docs[
                                                                              index]
                                                                          .id);
                                                                  titleController
                                                                      .clear();
                                                                  noteController
                                                                      .clear();
                                                                  Navigator.pop(
                                                                      context);
                                                                }
                                                                Fluttertoast.showToast(
                                                                    toastLength:
                                                                        Toast
                                                                            .LENGTH_LONG,
                                                                    msg:
                                                                        "Please fill in the note Title and note details");
                                                              },
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: const Icon(
                                              Icons.edit_outlined,
                                              size: 35,
                                              semanticLabel: "Edit",
                                              color: Colors.green,
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              FirebaseFirestore.instance
                                                  .collection("notes")
                                                  .doc(snapshot
                                                      .data!.docs[index].id)
                                                  .delete();
                                            },
                                            child: const Icon(
                                              Icons.delete_outlined,
                                              size: 35,
                                              color: Colors.red,
                                            ),
                                          )
                                        ],
                                      )),
                                  Positioned(
                                      bottom: 10,
                                      right: 5,
                                      child: Text(
                                          style: const TextStyle(
                                              fontSize: 11,
                                              color: Colors.blueGrey),
                                          "Added: ${DateFormat('yyyy-MM-dd').format(DateTime.parse(snapshot.data!.docs[index]['created'].toDate().toString()))}"))
                                ],
                              ),
                            ),
                          ));
                    }),
          );
        });
  }
}
