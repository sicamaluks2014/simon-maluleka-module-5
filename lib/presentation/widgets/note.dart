import 'package:flutter/material.dart';
import 'package:noted/data/models/note_model.dart';

class NoteTile extends StatelessWidget {
  NoteTile({Key? key}) : super(key: key);
  final NoteModel note = NoteModel.dummy();
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Text(note.id.toString()),
        title: Text(note.title),
        subtitle: Text(note.note),
        trailing: Text(note.created.toString()),
      ),
    );
  }
}
